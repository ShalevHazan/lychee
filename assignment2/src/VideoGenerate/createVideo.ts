import { createFFmpeg } from '@ffmpeg/ffmpeg';
import fs from 'fs'

export const createVideo = async (stringData: string) => {
    const ffmpegs = createFFmpeg({ log: true });
    let ffmpegLoadingPromise = ffmpegs.load();

    async function getFFmpeg() {
        if (ffmpegLoadingPromise) {
            await ffmpegLoadingPromise;
            ffmpegLoadingPromise = undefined;
        }
        return ffmpegs;
    }

    const dir = './output-outro';
    const inputFileName = 'input.mp4';
    const outputFileName = 'output-video.mp4';
    
    const ffmpeg = await getFFmpeg();

    try {
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir, { recursive: true });
         }

        const fontData = fs.readFileSync('OpenSans.ttf');
        ffmpeg.FS('writeFile', 'OpenSans.ttf', fontData);

        await ffmpeg.run(
            '-f', 'lavfi', '-i', 'color=c=black:s=720x1280:r=25',
            '-t', '20', inputFileName
            );

        await ffmpeg.run(
            '-i', inputFileName,
            '-ss', '00:00:00',
            '-t', '20',
            '-vf', `drawtext=text=${stringData}:fontfile=OpenSans.ttf:x=(main_w-text_w)/2:y=(main_h-text_h)/2:fontsize=32:fontcolor=0xFF00F2:enable='lt(t,4)'`,
            `${outputFileName}`
           );
      } catch (error) {
        console.error(`Error reading file: ${error}`);
      }

      const outputData = ffmpeg.FS('readFile', `${outputFileName}`);

      fs.writeFileSync(`${dir}/${outputFileName}`,outputData)

}