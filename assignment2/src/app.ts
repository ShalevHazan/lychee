import express from 'express';
import bodyParser from 'body-parser';
import {createVideo} from './VideoGenerate/createVideo'

const app = express();
const port = 8000;

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use(bodyParser.json())

app.post('/', async (req, res) => {
    const stringData = req.body.freeText !== '' ? req.body.freeText: req.body.selectChoice
    await createVideo(stringData)

    res.status(200).send('the video created');
})

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});