## assignment1

1. open the terminal on 'lychee\assignment1'
2. after you in this location, write npm i
3. after all the packages installed. write npm start

## assignment2

1. open the terminal on 'lychee\assignment2\src' (check you in src folder).
2. after you in this location' write npx tsx ./app.ts
3. you will see in terminal this text: "Express is listening at http://localhost:8000"

## work together

1. fill the free text or the select fields on the client side (assignment 1).
2. press on the "save" Button.
3. The proccess will take few seconds and after its done the video will be on this location: "lychee\assignment2\src\output-outro\output-video.mp4"
