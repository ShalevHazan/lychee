import React from "react";
import "./styles.css";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import { changeTitle } from "../../slices/rootSlice";
const KitName = () => {
  const title = useSelector((state: RootState) => state.root.title);
  const dispatch = useDispatch();

  return (
    <div className="content">
      <label>Brand kit name</label>
      <input
        value={title}
        onChange={(e) => dispatch(changeTitle(e.target.value))}
      />
    </div>
  );
};

export default KitName;
