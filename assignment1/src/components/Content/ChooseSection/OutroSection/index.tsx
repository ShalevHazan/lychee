import React, { useState } from "react";
import tooltipImage from "../../../../icons/tooltip.png";
import Tooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap.css";
import "./styles.css";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../../store";
import { changeSelect, changeFreeText } from "../../../../slices/rootSlice";
const OutroSection: React.FC = () => {
  // const [selectValue, setSelectValue] = useState("");
  const [count, setCount] = useState(0);

  const dispatch = useDispatch();
  const selectValue = useSelector(
    (state: RootState) => state.root.selectChoice
  );

  const handleChangeSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
    dispatch(changeSelect(event.target.value));
  };

  const freeText = useSelector((state: RootState) => state.root.freeText);

  const handleChangeFreeText = (
    event: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    setCount(event.target.value.length);
    if (selectValue) {
      handleClearSelect();
    }
    dispatch(changeFreeText(event.target.value));
  };

  const handleClearSelect = () => {
    dispatch(changeSelect(""));
  };
  return (
    <div className="outro-container">
      <div className="outro-tooltip-container">
        <label>Outro</label>
        <Tooltip
          placement="top"
          trigger={["hover"]}
          overlay={
            <span>We will show the call to action at the end of the clip</span>
          }
        >
          <img src={tooltipImage} width={16} />
        </Tooltip>
      </div>
      <div className="call-action-container">
        <label className="action-label">Call to action</label>
        <select required value={selectValue} onChange={handleChangeSelect}>
          <option value="" hidden>
            Select
          </option>
          <option value="Listen on Spotify"> Listen on Spotify</option>

          <option value="Listen on Apple">Listen on Apple</option>

          <option value="Listen on Google">Listen on Google</option>
        </select>
      </div>
      <div className="call-action-container">
        <label className="action-label">Custom call to action</label>
        <div className="textarea-container">
          <textarea
            maxLength={20}
            value={freeText}
            onChange={handleChangeFreeText}
          ></textarea>
          <div className="count">{count} / 20</div>
        </div>
      </div>
    </div>
  );
};

export default OutroSection;
