import React, { FormEventHandler, useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import OutroSection from "./OutroSection";
import "./styles.css";

const ChooseSection: React.FunctionComponent = () => {
  const tabs = ["texts", "Logo", "Outro", "Custom brand kit"];
  const [sectionIndex, setSectionIndex] = useState(2);

  return (
    <Tabs
      className="tabs-container"
      defaultIndex={sectionIndex}
      onSelect={(k) => {
        setSectionIndex(k);
      }}
    >
      <TabList className="tabs" aria-orientation="vertical">
        {tabs.map((ele, index) => (
          <Tab key={index} className={sectionIndex === index ? "active" : ""}>
            {ele}
          </Tab>
        ))}
      </TabList>
      <TabPanel></TabPanel>
      <TabPanel></TabPanel>
      <TabPanel className="panel-container">
        <OutroSection />
      </TabPanel>
      <TabPanel></TabPanel>
    </Tabs>
  );
};

export default ChooseSection;
