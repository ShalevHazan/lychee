import React, { useState } from "react";
import ChooseSection from "./ChooseSection";
import "./styles.css";
const Content = () => {
  return (
    <div className="content-container">
      <ChooseSection />
    </div>
  );
};

export default Content;
