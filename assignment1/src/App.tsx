import React from "react";
import KitName from "./components/KitName";
import Content from "./components/Content";
import "./App.css";
import { useSelector } from "react-redux";
import { RootState } from "./store";
import { postData } from "./functions/axiosFunctions/requests";

function App() {
  const title = useSelector((state: RootState) => state.root.title);
  const selectChoice = useSelector(
    (state: RootState) => state.root.selectChoice
  );
  const freeText = useSelector((state: RootState) => state.root.freeText);
  return (
    <div className="App">
      <KitName />
      <Content />
      <hr />
      <button
        onClick={() =>
          postData({
            title: title,
            selectChoice: selectChoice,
            freeText: freeText,
          })
        }
      >
        Send
      </button>
    </div>
  );
}

export default App;
