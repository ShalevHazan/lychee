import { createSlice } from "@reduxjs/toolkit";

type InitialState = {
  title: string;
  selectChoice: string;
  freeText: string;
};

const initialState: InitialState = {
    title: '',
    selectChoice: '',
    freeText: '',
};

export const rootSlice = createSlice({
  name: "formData",
  initialState: initialState,
  reducers: {
    changeTitle: (
      state,
      action: {
        payload: string;
        type: string;
      }
    ) => {
      return {
        ...state,
        title: action.payload,
      };
    },
    changeSelect: (
        state,
        action: {
          payload: string;
          type: string;
        }
      ) => {
        return {
          ...state,
          selectChoice: action.payload,
        };
      },
      changeFreeText: (
        state,
        action: {
          payload: string;
          type: string;
        }
      ) => {
        return {
          ...state,
          freeText: action.payload,
        };
      },
  },
});

export const { changeTitle, changeSelect, changeFreeText } = rootSlice.actions;
export default rootSlice.reducer;
