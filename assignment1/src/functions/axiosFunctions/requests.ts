import axios from "axios";

const serverPort = 8000
const serverUrl = 'http://localhost'

interface dataprops {title: string, selectChoice: string, freeText: string}

export const postData = (data: dataprops) => {
    const stringData = JSON.stringify(data);
    axios.post(`${serverUrl}:${serverPort}/`, data, {
        headers: {
          'Content-Type': 'application/json'
        },
      })
        .then(function (response) {
          console.log(response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
}